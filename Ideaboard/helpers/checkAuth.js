import { unauthorized } from "../errors/unauthorized";
import jwt from "jsonwebtoken";
import env from "dotenv";
env.config();

export const checkAuth = (ctx) => {
  if (ctx.request.headers && !ctx.request.headers.authorization) {
    ctx.body = unauthorized(ctx, "Unauthorized request");
    return 0;
  }
  const token = ctx.request.headers.authorization.split(" ")[1];
  if (!token) {
    ctx.body = unauthorized(ctx, "Access denied. Invalid token provided.");
    return 0;
  }

  let decoded;
  try {
    decoded = jwt.verify(token, process.env.JWT_SECRET);
  } catch (err) {
    ctx.body = unauthorized(ctx, "Invalid token!");
    return 0;
  }
  return decoded.id;
};
