export const badRequest = (ctx, message) => {
  ctx.status = 400;
  return { success: false, message };
};
