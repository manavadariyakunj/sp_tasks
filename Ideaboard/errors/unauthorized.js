export const unauthorized = (ctx, message) => {
  ctx.status = 401;
  return { success: false, message };
};
