export const notFound = (ctx, message) => {
  ctx.status = 404;
  return { success: false, message };
};
