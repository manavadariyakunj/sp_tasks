import { badRequest } from "../errors/badRequest";

export const validateEmail = (ctx) => {
  const email =
    ctx.request.body && ctx.request.body.email && ctx.request.body.email.trim();

  if (!email) return badRequest(ctx, "Email is required.");

  const emailRegex = /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$/;
  if (!emailRegex.test(email))
    return badRequest(ctx, "Email is in incorrect format.");

  return { success: true };
};
