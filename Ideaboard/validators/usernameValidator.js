import { badRequest } from "../errors/badRequest";

export const validateUsername = (ctx) => {
  const username =
    ctx.request.body &&
    ctx.request.body.username &&
    ctx.request.body.username.trim();
  if (!username) {
    return badRequest(ctx, "Username is required.");
  }
  const usernameRegex = /^[a-zA-Z0-9._]{4,15}$/;
  if (!usernameRegex.test(username)) {
    return badRequest(ctx, "Username is in incorrect format.");
  }
  return { success: true };
};
