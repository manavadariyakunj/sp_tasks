import { badRequest } from "../errors/badRequest";

export const validatePwd = (ctx, next) => {
  const pwd =
    ctx.request.body &&
    ctx.request.body.password &&
    ctx.request.body.password.trim();
  if (!pwd) {
    return badRequest(ctx, "Password is required.");
  }
  const pwdRegex =
    /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%^&*()])[a-zA-Z\d!@#$%^&*()]{8,}$/;
  if (!pwdRegex.test(pwd)) {
    return badRequest(
      ctx,
      "Password should contain at least one lowercase, one uppercase, one digit, and one of the special characters (!,@,#,$,%,^,&,*,(,))."
    );
  }
  return { success: true };
};
