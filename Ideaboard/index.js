import Koa from "koa";
import bodyParser from "koa-bodyparser";
import { userRouter } from "./routes/userRoutes";
import { boardRouter } from "./routes/boardRoutes";
import { db, connectToDb } from "./db";
const app = new Koa();

app.use(bodyParser());

app.use(userRouter.routes()).use(userRouter.allowedMethods());
app.use(boardRouter.routes()).use(boardRouter.allowedMethods());

connectToDb(db)
  .then(() => {
    console.log("Database Connected...");
    app.listen(3000, () => {
      console.log("Server running on http://localhost:3000");
    });
  })
  .catch((err) => {
    console.log(err);
  });
