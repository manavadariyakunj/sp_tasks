import { ObjectID } from "bson";
import { db } from "../db";
import { findUserById } from "./userQueries";

export const insertBoard = async (data) => {
  await db.collection("boards").insertOne(data);
};

export const findAllBoardsForUser = async (userId) => {
  return await findUserById(userId);
};

export const findBoardWithId = async (boardId, ownerId) => {
  const res = await db
    .collection("boards")
    .findOne({ _id: boardId, owner_id: ownerId });
  const count = await db
    .collection("boards")
    .count({ _id: boardId, owner_id: ownerId });
  return { ...res, count };
};

export const deleteBoard = async (board) => {
  await db.collection("boards").deleteOne({ _id: board._id });
  await db
    .collection("users")
    .updateOne({ _id: board.owner_id }, { $pull: { boards: board._id } });
  return true;
};

export const getOwnedBoards = async (ownerId) =>
  await db.collection("boards").find({ owner_id: ownerId }).toArray();

export const getDuplicateBoards = async (boardId, userId, updatedTitle) =>
  await db.collection("boards").count({
    $and: [
      { title: updatedTitle, owner_id: userId },
      { _id: { $ne: boardId } },
    ],
  });

export const updateBoardDetails = async (boardId, updatedBoard) => {
  delete updatedBoard._id &&
    (await db.collection("boards").updateOne(
      { _id: boardId },
      {
        $set: updatedBoard,
      },
      { upsert: false }
    ));
  return true;
};

export const isAlreadyBoard = async (title, ownerId) =>
  await db
    .collection("boards")
    .count({ title, owner_id: new ObjectID(ownerId) });

export const pushBoardToUser = async (ownerId, data) => {
  await db
    .collection("users")
    .updateOne({ _id: ownerId }, { $push: { boards: data._id } });
};

export const deleteBoardWithId = async (boardId) => {
  await db.collection("boards").deleteOne({ _id: boardId });
};
