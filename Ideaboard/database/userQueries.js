import { db } from "../db";

//newfn: Insert user in db
export const insertUser = async (user) => {
  await db.collection("users").insertOne(user);
  return true;
};

//newfn: Check if user is present in db
export const isUser = async (email, username) =>
  await db.collection("users").findOne({ email: email, username: username });

//newfn: Check if user with given credentials already exists
export const userExists = async (email, username) =>
  await db.collection("users").count({ $or: [{ email }, { username }] });

//newfn: Find user with given username as id
export const findUserById = async (id) =>
  await db.collection("users").findOne({ username: id });

//newfn: Get list of all users (without password)
export const getAllUsers = async () =>
  await db
    .collection("users")
    .find({}, { projection: { password: 0 } })
    .toArray();

//newfn: Check if a user is already present with the to-update credentials
export const isUniqueUser = async (userId, updatedUsername, updatedEmail) =>
  await db.collection("users").count({
    $and: [
      { $or: [{ username: updatedUsername }, { email: updatedEmail }] },
      { _id: { $ne: userId } },
    ],
  });

//newfn: Update the user
export const updateUserDetails = async (userId, updatedUser) => {
  delete updatedUser._id &&
    (await db
      .collection("users")
      .updateOne({ _id: userId }, { $set: updatedUser }, { upsert: false }));
  return true;
};

//newfn: Delete a user with given username as id (no need to pass username as id as we can have access to user id on ctx.state and remove user with that very id.)
export const delUser = async (id) => {
  await db.collection("users").deleteOne({ username: id });
  return true;
};
