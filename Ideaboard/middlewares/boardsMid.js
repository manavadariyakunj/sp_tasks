import { ObjectID } from "bson";
import {
  findBoardWithId,
  getDuplicateBoards,
  isAlreadyBoard,
} from "../database/boardQueries";
import { findUserById } from "../database/userQueries";
import { badRequest } from "../errors/badRequest";

export const checkDuplicateBoard = async (ctx, next) => {
  const title = ctx.request.body.title.trim();
  const { id } = ctx.state;
  if (!title) {
    ctx.body = badRequest(ctx, "Title is required");
    return;
  }
  const res = await isAlreadyBoard(title, id);
  if (res) {
    ctx.body = badRequest(
      ctx,
      "A board with the same title is already present. Give a different title."
    );
    return;
  }
  return next();
};

export const verifyBoardRegex = async (ctx, next) => {
  const specialCharRegex = /[!@#$%^&*(),.?":{}|<>]/;
  const maxTitleLength = 50;
  const maxDescriptionLength = 200;
  const { title, description } = ctx.request.body;
  if (title.length > maxTitleLength) {
    ctx.body = badRequest(ctx, "Title should not exceed 50 characters.");
    return;
  }
  if (description && description.length > maxDescriptionLength) {
    ctx.body = badRequest(ctx, "Description should not exceed 200 characters.");
    return;
  }
  if (specialCharRegex.test(title)) {
    ctx.body = badRequest(
      ctx,
      "Title should not contain any special characters"
    );
    return;
  }
  return next();
};

export const userExists = async (ctx, next) => {
  const userId = ctx.request.params.id;
  const user = await findUserById(userId);
  if (!user) {
    ctx.body = badRequest(ctx, "No user found with the given ID.");
    return;
  }
  ctx.state.listBoardsForUserId = user._id;
  return next();
};

export const verifyBoard = async (ctx, next) => {
  let boardId;
  try {
    boardId = new ObjectID(ctx.request.params.id);
  } catch (err) {
    ctx.body = badRequest(ctx, "Invalid ID. Please check ID and try again.");
    return;
  }
  const board = await findBoardWithId(boardId, ctx.state.id);
  if (!board.count) {
    ctx.body = badRequest(ctx, "No board found with given ID!");
    return;
  }
  ctx.state.board = board;
  return next();
};

export const isUniqueBoard = async (ctx, next) => {
  const updBoard = ctx.request.body;
  const foundBoard = ctx.state.board;
  const res = await getDuplicateBoards(
    foundBoard._id,
    foundBoard.owner_id,
    updBoard.title
  );
  if (res) {
    ctx.body = badRequest(ctx, "Board with this title already exists.");
    return 0;
  }
  await next();
};
