import md5 from "md5";
import {
  findUserById,
  isUniqueUser,
  isUser,
  userExists,
} from "../database/userQueries";
import { badRequest } from "../errors/badRequest";
import { notFound } from "../errors/notFound";
import { checkAuth } from "../helpers/checkAuth";
import { ObjectID } from "bson";

//newfn: Verify the logged in user
export const verifyUser = (ctx, next) => {
  const decodedId = checkAuth(ctx);
  if (!decodedId) return;
  ctx.state.id = new ObjectID(decodedId);
  return next();
};

//newfn: Verify login details of user and generate a token
export const verifyLogin = async (ctx, next) => {
  const user = ctx.request.body;
  const foundUser = await isUser(user.email.trim(), user.username.trim());
  if (!foundUser) {
    ctx.body = badRequest(ctx, "Invalid username, email or password!");
    return;
  }
  const isPasswordValid = md5(user.password) === foundUser.password;
  if (!isPasswordValid) {
    ctx.body = badRequest(ctx, "Invalid username, email or password!");
    return;
  }
  ctx.state = { id: foundUser._id, email: foundUser.email };
  return next();
};

//newfn: Check that the given credentials conflict with existing users
export const verifyUserDuplicates = async (ctx, next) => {
  const { email, username } = ctx.request.body;
  const res = await userExists(email, username);
  if (res) {
    ctx.body = badRequest(
      ctx,
      "User with these credentials already exists. Provide a different username or email!"
    );
    return;
  }
  return next();
};

//newfn: All in one validator which takes array of validators and execute them and store array of errors and show them
export const validator = (validators) => {
  return async (ctx, next) => {
    const errors = [];

    for (const validator of validators) {
      const result = await validator(ctx);
      if (!result.success) {
        errors.push(result.message);
      }
    }

    if (errors.length > 0) {
      ctx.status = 400;
      ctx.body = { success: false, errors };
    } else {
      await next();
    }
  };
};

//newfn: Check if user is verified and is the owner of the account
export const foundAndIsOwner = async (ctx, next) => {
  const id = ctx.request.params.id;
  const owner_id = ctx.state.id;
  const foundUser = await findUserById(id);
  ctx.state.foundUser = foundUser;
  if (!foundUser) {
    ctx.body = notFound(ctx, "No user found with given ID!");
  } else if (foundUser._id && !foundUser._id.equals(owner_id)) {
    ctx.body = badRequest(ctx, "You are not signed in with the given account!");
  } else {
    return next();
  }
};

//newfn: Check if given credentials for updating user conflicts with an existing user
export const isUnique = async (ctx, next) => {
  const updUser = ctx.request.body;
  const owner_id = ctx.state.id;
  const res = await isUniqueUser(owner_id, updUser.username, updUser.email);
  if (res) {
    ctx.body = badRequest(ctx, "User with these credentials already exists.");
    return;
  }
  return next();
};
