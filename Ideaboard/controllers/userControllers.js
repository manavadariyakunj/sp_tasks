import jwt from "jsonwebtoken";
import md5 from "md5";
import env from "dotenv";
import {
  getAllUsers,
  delUser,
  updateUserDetails,
  insertUser,
} from "../database/userQueries";
import { deleteBoardWithId } from "../database/boardQueries";
env.config();

export const message = (success, text) => ({ success, message: text });

//newfn: Create User
export const newUser = async (ctx) => {
  const user = {};
  user.username = ctx.request.body.username;
  user.email = ctx.request.body.email;
  user.password = md5(ctx.request.body.password);
  user.joined_at = new Date(new Date().getTime() + 19800000);
  user.boards = [];
  (await insertUser(user)) &&
    (ctx.body = {
      success: true,
      message: "Successfully inserted user.",
    });
};

//newfn: User Login
export const loginUser = async (ctx) => {
  const { id, email } = ctx.state;
  const token = jwt.sign({ id, email }, process.env.JWT_SECRET, {
    expiresIn: "20m",
  });
  const res = message(true, "Successfully logged you in!");
  ctx.body = {
    ...res,
    token,
  };
};

//newfn: List All Users
export const allUsers = async (ctx) => {
  const data = await getAllUsers();
  ctx.body = { success: true, data };
};

//newfn: Update User
export const updateUser = async (ctx) => {
  const updUser = ctx.request.body;
  const owner_id = ctx.state.id;
  const update = await updateUserDetails(owner_id, updUser);
  update && (ctx.body = message(true, "Successfully updated user details"));
};

//newfn: Delete User
export const deleteUser = async (ctx) => {
  const id = ctx.request.params.id;
  const { foundUser } = ctx.state;
  for (let boardId of foundUser.boards) {
    await deleteBoardWithId(boardId);
  }
  const res = await delUser(id);
  res &&
    (ctx.body = message(true, `Successfully deleted the user with id ${id}!`));
};
