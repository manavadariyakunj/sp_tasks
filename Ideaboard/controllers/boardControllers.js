import { checkAuth } from "../helpers/checkAuth";
import env from "dotenv";
import { ObjectID } from "bson";
import {
  deleteBoard,
  getOwnedBoards,
  insertBoard,
  pushBoardToUser,
  updateBoardDetails,
} from "../database/boardQueries";
env.config();

//newfn: Create new board.
export const newUserBoard = async (ctx) => {
  const data = ctx.request.body;
  const owner_id = ctx.state.id;
  if (!data.description) data.description = "";
  const userId = checkAuth(ctx);
  if (!userId) return;
  userId && (data.owner_id = new ObjectID(userId));
  data.pins = [];
  data.collaborators = [];

  data.created_at = new Date(new Date().getTime() + 19800000);
  data.updated_at = new Date(new Date().getTime() + 19800000);

  await insertBoard(data);
  await pushBoardToUser(owner_id, data);
  ctx.body = {
    success: true,
    message: "Successfully created new board.",
  };
};

//newfn: List all boards for a particular user
export const listUserBoards = async (ctx) => {
  const userId = ctx.state.listBoardsForUserId;
  const data = await getOwnedBoards(userId);
  data &&
    (ctx.body = {
      success: true,
      boards: data,
    });
};

//newfn: Update board with board id
export const updateUserBoard = async (ctx) => {
  const updBoard = ctx.request.body;
  const foundBoard = ctx.state.board;
  const res = await updateBoardDetails(foundBoard._id, updBoard);
  res &&
    (ctx.body = {
      success: true,
      message: "Successfully updated board details.",
    });
};

//newfn: Delete board with board id
export const deleteUserBoard = async (ctx) => {
  const board = ctx.state.board;
  (await deleteBoard(board)) &&
    (ctx.body = {
      success: true,
      message: "Successfully deleted board!",
    });
};
