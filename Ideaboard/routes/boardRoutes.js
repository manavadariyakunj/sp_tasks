import koaRouter from "koa-router";
const router = new koaRouter();
import { verifyUser } from "../middlewares/usersMid";
import {
  newUserBoard,
  listUserBoards,
  deleteUserBoard,
  updateUserBoard,
} from "../controllers/boardControllers";
import {
  checkDuplicateBoard,
  isUniqueBoard,
  userExists,
  verifyBoard,
  verifyBoardRegex,
} from "../middlewares/boardsMid";

router.get(
  "/boards/new",
  verifyUser,
  checkDuplicateBoard,
  verifyBoardRegex,
  newUserBoard
);

router.get("/:id/boards", verifyUser, userExists, listUserBoards);

router.patch(
  "/boards/:id",
  verifyUser,
  verifyBoardRegex,
  verifyBoard,
  isUniqueBoard,
  updateUserBoard
);

router.delete("/boards/:id", verifyUser, verifyBoard, deleteUserBoard);

export { router as boardRouter };
