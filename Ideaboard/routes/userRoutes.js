import koaRouter from "koa-router";
import {
  verifyUser,
  verifyLogin,
  verifyUserDuplicates,
  validator,
  foundAndIsOwner,
  isUnique,
} from "../middlewares/usersMid";
const router = new koaRouter();
import {
  allUsers,
  deleteUser,
  loginUser,
  newUser,
  updateUser,
} from "../controllers/userControllers";
import { validateEmail } from "../validators/emailValidator";
import { validateUsername } from "../validators/usernameValidator";
import { validatePwd } from "../validators/pwdValidator";

router.get("/users", verifyUser, allUsers);

router.post(
  "/register",
  validator([
    (body) => validateUsername(body),
    (body) => validateEmail(body),
    (body) => validatePwd(body),
  ]),
  verifyUserDuplicates,
  newUser
);

router.post(
  "/login",
  validator([
    (body) => validateUsername(body),
    (body) => validateEmail(body),
    (body) => validatePwd(body),
  ]),
  verifyLogin,
  loginUser
);

router.patch(
  "/user/:id",
  verifyUser,
  validator([(body) => validateUsername(body), (body) => validateEmail(body)]),
  foundAndIsOwner,
  isUnique,
  updateUser
);

router.delete("/user/:id", verifyUser, foundAndIsOwner, deleteUser);

export { router as userRouter };
